import numpy as np
import treecorr


def calculate_xi_wrapper(catalog, rand_catalog, min_sep=1e-2, max_sep=40, nbins=40):
    """
    Wraps treecorr to calculate spatial correlation function xi.

    catalog: np.ndarray
        array with shape (Ngal, 3) whose columns contain x, y and z of galaxies, in Mpc/h
    rand_catalog: np.ndarray
        same as catalog, but for random sample
    min_sep: float
        minimum value of interval to calculate xi
    max_sep: float
        maximum value of interval to calculate xi
    nbins: int
        number of bins of interval to calculate xi

    returns:
        r, xi
        r: np.ndarray
            array with position of mean point of bins used to calculate xi
        xi: np.array
            array with calculated values of xi
    """
    Ngals, dgals = catalog.shape
    Nrand, drand = catalog.shape
    if dgals != 3:
        raise ValueError("catalog array shape should be (N, 3)")
    if drand != 3:
        raise ValueError("rand_catalog array shape should be (N, 3)")
    if Ngals == 0:
        raise ValueError("No galaxies in catalog array!")
    if Nrand == 0:
        raise ValueError("No randoms in rand_catalog array!")

    gg = treecorr.NNCorrelation(min_sep=min_sep, max_sep=max_sep, nbins=nbins)
    gr = treecorr.NNCorrelation(min_sep=min_sep, max_sep=max_sep, nbins=nbins)
    rr = treecorr.NNCorrelation(min_sep=min_sep, max_sep=max_sep, nbins=nbins)

    tc_catalog = treecorr.Catalog(x=catalog[:,0], y=catalog[:,1], z=catalog[:,2])
    tc_rand_catalog = treecorr.Catalog(x=rand_catalog[:,0], y=rand_catalog[:,1], z=rand_catalog[:,2])

    gg.process(tc_catalog)
    gr.process(tc_catalog, tc_rand_catalog)
    rr.process(tc_rand_catalog)

    xi, _ = gg.calculateXi(rr, gr)
    r = np.exp(gg.meanlogr)

    return r, xi


def calculate_acf_wrapper(catalog, rand_catalog, min_sep=1e-2, max_sep=40, nbins=40):
    """
    Wraps treecorr to calculate angular correlation function.

    catalog: np.ndarray
        array with shape (Ngal, 2) whose columns contain ra and dec, in radians
    rand_catalog: np.ndarray
        same as catalog, but for random sample
    min_sep: float
        minimum value of interval to calculate acf
    max_sep: float
        maximum value of interval to calculate acf
    nbins: int
        number of bins of interval to calculate acf

    returns:
        theta, acf
        theta: np.ndarray
            array with angle of mean point of bins used to calculate acf
        acf: np.array
            array with calculated values of acf
    """
    Ngals, dgals = catalog.shape
    Nrand, drand = catalog.shape
    if dgals != 2:
        raise ValueError("catalog array shape should be (N, 2)")
    if drand != 2:
        raise ValueError("rand_catalog array shape should be (N, 2)")
    if Ngals == 0:
        raise ValueError("No galaxies in catalog array!")
    if Nrand == 0:
        raise ValueError("No randoms in rand_catalog array!")

    gg = treecorr.NNCorrelation(min_sep=min_sep, max_sep=max_sep, nbins=nbins)
    gr = treecorr.NNCorrelation(min_sep=min_sep, max_sep=max_sep, nbins=nbins)
    rr = treecorr.NNCorrelation(min_sep=min_sep, max_sep=max_sep, nbins=nbins)

    tc_catalog = treecorr.Catalog(ra=catalog[:,0], dec=catalog[:,1], ra_units="rad", dec_units="rad")
    tc_rand_catalog = treecorr.Catalog(ra=rand_catalog[:,0], dec=rand_catalog[:,1], ra_units="rad", dec_units="rad")

    gg.process(tc_catalog)
    gr.process(tc_catalog, tc_rand_catalog)
    rr.process(tc_rand_catalog)

    acf, _ = gg.calculateXi(rr, gr)
    theta = np.exp(gg.meanlogr)

    return theta, acf


def sample_randoms(nrandoms, limits=[(0, 2*np.pi), (-np.pi/2, np.pi/2)]):
    """
    Samples nrandoms points over the sphere. Returns ra and dec in rad.

    nrandoms: int
        number of points to sample
    limits: list of 2-tuples [(ra_min, ra_max), (dec_min, dec_max)]
        limits of square region to sample

    returns:
        np.ndarray, shape (nrandoms, 2)
            array with ra and dec of sampled points, in radians
    """
    (ra_min, ra_max), (dec_min, dec_max) = limits
    sin_min, sin_max = np.sin(dec_min), np.sin(dec_max)

    ra = np.random.random(nrandoms) * (ra_max - ra_min) + ra_min
    dec = np.arcsin( np.random.random(nrandoms) * (sin_max - sin_min) + sin_min )

    return np.c_[ra, dec]
